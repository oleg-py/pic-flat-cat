name := "pic-flat-cat"
scalaVersion := "2.11.7"

sourceDirectory in Compile <<= baseDirectory(_ / "src")
scalaSource in Compile <<= sourceDirectory in Compile
resourceDirectory in Compile <<= baseDirectory(_ / "resources")

libraryDependencies ++= Seq(
  "org.scalafx" %% "scalafx" % "8.0.60-R9",
  "com.github.scala-incubator.io" %% "scala-io-core" % "0.4.3",
  "com.github.scala-incubator.io" %% "scala-io-file" % "0.4.3",
  "io.reactivex" %% "rxscala" % "0.26.0",

  "com.typesafe" % "config" % "1.3.0",

  "org.scala-lang.modules" %% "scala-parser-combinators" % "1.0.4",

  "net.java.dev.jna" % "jna" % "4.2.1",
  "net.java.dev.jna" % "jna-platform" % "4.2.1"
)


artifact in (Compile, assembly) := {
  val art = (artifact in (Compile, assembly)).value
  art.copy(`classifier` = Some("assembly"))
}

addArtifact(artifact in (Compile, assembly), assembly)
checksums in publish := Seq()
publishArtifact in packageBin := false
publishArtifact in packageDoc := false
publishArtifact in packageSrc := false
publishMavenStyle := false

releaseUseGlobalVersion := false
import ReleaseTransformations._
releaseProcess := Seq[ReleaseStep](
  checkSnapshotDependencies,
  inquireVersions,
  runTest,
  setReleaseVersion,
  commitReleaseVersion,
  tagRelease,
  publishArtifacts,
  setNextVersion,
  commitNextVersion
)

publishTo := Some(Resolver.file("file", file("D:/opt/pic-flat-cat"))(Patterns("[artifact]-[revision](-[classifier]).[ext]")) )

scalacOptions ++= Seq(
  "-deprecation",
  "-feature"
)

crossPaths := false