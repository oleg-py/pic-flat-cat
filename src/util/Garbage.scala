package util


trait Garbage {
  Garbage.register(this)
  def cleanUp(): Unit
}

object Garbage {
  private[this] var set = Set.empty[Garbage]
  private def register(g: Garbage) = synchronized { set = set + g }

  def cleanUp() = set.foreach(_.cleanUp())
}