package util

import javafx.beans.property.SimpleObjectProperty
import javafx.beans.value.{ChangeListener, ObservableValue}
import javafx.geometry.{HPos, VPos}
import javafx.scene.image.ImageView
import javafx.scene.layout.Region


class JImageViewPane(iv: ImageView) extends Region {
  val imageViewProperty = new SimpleObjectProperty[ImageView]()

  imageViewProperty.addListener(new ChangeListener[ImageView]() {
    override def changed(arg0: ObservableValue[_ <: ImageView] , oldIV: ImageView , newIV: ImageView) {
      if (oldIV != null) {
        getChildren.remove(oldIV)
      }
      if (newIV != null) {
        getChildren.add(newIV)
      }
    }
  })
  this.imageViewProperty.set(iv)


  def this() = this(new ImageView())

  protected override def layoutChildren() {
    val imageView = imageViewProperty.get()
    if (imageView != null) {
      imageView.setFitWidth(getWidth)
      imageView.setFitHeight(getHeight)
      layoutInArea(imageView, 0, 0, getWidth, getHeight, 0, HPos.CENTER, VPos.CENTER)
    }
    super.layoutChildren()
  }
}
