package util

import scala.collection.SeqLike

object CollectionConversions {
  implicit class EnhancedSeq[A, Repr <: SeqLike[A, Repr]](val coll: SeqLike[A, Repr]) extends AnyVal {
    def sortedChunksOn[B](cond: A => B)(implicit ord: Ordering[B]) = {
      coll.sortBy(cond).chunksOn(cond)
    }

    def chunksOn[B](cond: A => B): Iterator[Repr] = {
      if (coll.isEmpty) Iterator.empty
      else {
        def nextChunk(iter: BufferedIterator[A]): Stream[Repr] = {
          if (iter.isEmpty) Stream.empty
          else {
            val builder = coll.seq.companion.newBuilder[A]
            builder += iter.head
            val ref = cond(iter.head)
            iter.next()
            while (iter.hasNext && cond(iter.head) == ref) {
              builder += iter.next()
            }
            builder.result().asInstanceOf[Repr] #:: nextChunk(iter)
          }
        }

        nextChunk(coll.iterator.buffered).iterator
      }
    }
  }
}
