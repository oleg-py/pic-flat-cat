import java.io.File

import com.typesafe.config.ConfigFactory

package object util {
  val Config = {
    val file = new File("./pic-flat-cat.conf")
    if (file.exists()) ConfigFactory.load(ConfigFactory.parseFile(file))
    else ConfigFactory.load()
  }
}
