package util

import java.{lang => jln}
import javafx.beans.binding.Bindings
import javafx.collections.ObservableList
import javafx.scene.{Node => jfxNode}

import scalafx.application.Platform
import scalafx.beans.property.{DoubleProperty, BooleanProperty, ObjectProperty}
import scalafx.beans.value.ObservableValue
import scalafx.collections.ObservableBuffer
import scalafx.delegate.SFXDelegate
import scalafx.{collections => sfxc}

import rx.lang.scala.Observable

object ObservableConversions {
  implicit class ObservableToObject[A](val self: Observable[A]) extends AnyVal {
    def objProp: ObservableValue[A, A] = {
      val prop = new ObjectProperty[A]()
      self foreach { x => Platform.runLater { prop.value = x } }
      prop
    }

    def boolProp(implicit ev: A => Boolean): ObservableValue[Boolean, jln.Boolean] = {
      val prop = new BooleanProperty()
      self foreach { x => Platform.runLater { prop.value = x } }
      prop
    }

    def doubleProp(implicit ev: A => Double): ObservableValue[Double, jln.Number] = {
      val prop = new DoubleProperty()
      self foreach { x => Platform.runLater { prop.value = x } }
      prop
    }

    def listProp[B](implicit ev1: A <:< Seq[B]): ObservableBuffer[B] = {
      val prop = new ObservableBuffer[B]()
      self foreach { xs => sfxc.fillCollection(prop, ev1(xs)) }
      prop
    }

    def fxChildrenProp(implicit ev1: A <:< Seq[SFXDelegate[jfxNode]]): ObservableList[jfxNode] = {
      val prop = new ObservableBuffer[jfxNode]()
      self foreach { xs => Platform.runLater { sfxc.fillCollection(prop, xs map {_.delegate})} }
      prop
    }
  }

  implicit class ObservableListOperations[A](val self: ObservableList[A]) extends AnyVal {
    def <==(other: ObservableBuffer[A]): Unit = <==(other.delegate)
    def <==(other: ObservableList[A]): Unit = Bindings.bindContent(self, other)
  }
}
