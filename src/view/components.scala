package view

import java.io.{PrintWriter, StringWriter}
import javafx.scene.{image => jfsi}

import scalafx.Includes._
import scalafx.application.JFXApp.PrimaryStage
import scalafx.application.Platform
import scalafx.geometry.Insets
import scalafx.geometry.Rectangle2D
import scalafx.scene.Scene
import scalafx.scene.control._
import scalafx.scene.image.{Image, ImageView}
import scalafx.scene.input.{KeyCode, KeyEvent}
import scalafx.scene.layout.{HBox, Priority, VBox}
import scalafx.scene.text.{Font, Text}

import model.timetravel.direction._
import model.transitions._
import model.{MatchKey, SortingTimeline}
import rx.lang.scala.Subject
import rx.lang.scala.subjects.PublishSubject
import util.CollectionConversions.EnhancedSeq
import util.ObservableConversions._
import util._

object components {
  val IconSize = 24
  def icon(res: String) = new ImageView(new Image(getClass.getResourceAsStream(res))) {
    smooth = true
    preserveRatio = true
    fitWidth = IconSize
    fitHeight = IconSize
  }
  def toolbar(tl: SortingTimeline, stran: Subject[StateTransition], ttran: Subject[TimeDirection]) = new HBox {
    spacing = 10
    children = Seq(
      new Button {
        graphic = icon("/icons/undo.png")
        prefWidth = IconSize
        prefHeight = IconSize
        disable <== tl.undoDisabled.boolProp
        onAction = handle { ttran.onNext(Backward) }
      },
      new Button {
        graphic = icon("/icons/redo.png")
        prefWidth = IconSize
        prefHeight = IconSize
        disable <== tl.redoDisabled.boolProp
        onAction = handle { ttran.onNext(Forward) }
      },
      new Button {
        graphic = icon("/icons/skip.png")
        prefWidth = IconSize
        prefHeight = IconSize
        disable <== tl.skipDisabled.boolProp
        onAction = handle { stran.onNext(Skip) }
      },
      new Button {
        graphic = icon("/icons/discard.png")
        prefWidth = IconSize
        prefHeight = IconSize
        disable <== tl.sortDisabled.boolProp
        onAction = handle { stran.onNext(Discard) }
      },
      new Button {
        graphic = icon("/icons/refresh.png")
        prefWidth = IconSize
        prefHeight = IconSize
        onAction = handle { ttran.onNext(Start) }
      }
    )
  }

  def progressbar(tl: SortingTimeline) = {
    val pr = tl.processed.combineLatestWith(tl.total)((proc, total) => proc.toDouble / total)
    new ProgressBar {
      progress <== pr.doubleProp

      maxWidth = Double.MaxValue
    }
  }

  def filename(tl: SortingTimeline) = {
    new Text {
      text <== tl.fileName.map(_.map(s =>
        if (s.length > 33) s.take(30) + "..." else s
      ).getOrElse("No file")).objProp
      font = new Font(Config.getString("ui.file.font"), Config.getDouble("ui.file.size"))
    }
  }

  def imageView(tl: SortingTimeline) = {
    val img = tl.fileURL.map(_.map(u => new jfsi.Image(u.toExternalForm)).orNull)

    new ImageViewPane (
      new ImageView {
        preserveRatio = true
        smooth = true
        image <== img.objProp
      }
    ) {
      hgrow = Priority.Always
      vgrow = Priority.Always
    }
  }

  private[this] val regex = """(.+)\s!*-\s!*(.+)""".r

  def categorySelector(tl: SortingTimeline, sub: Subject[StateTransition]) = {
    val emptyStringLast = new Ordering[String] {
      override def compare(x: String, y: String) =
        if (x.isEmpty) { if (y.isEmpty) 0 else 1 }
        else { if (y.isEmpty) -1 else x.compareTo(y) }
    }

    val clickChangeEnabled = Config.getBoolean("ui.color.enabled")
    val clickCountRequired = Config.getLong("ui.color.clicks-before")
    def colorCategory(s: String) = (s.## & 0xFFFFFF).toHexString.padTo(6, '0')
    def colorOf(clicksOf: String => Long, s: String) =
      if (!clickChangeEnabled || clicksOf(s) < clickCountRequired) null
      else s"-fx-base: #${colorCategory(s)}; -fx-font-weight: bold"

    tl.cagetories.map{ case (allCats, clicksOf) =>
      allCats
        .sortedChunksOn(s => regex.unapplySeq(s).map{_.head}.getOrElse(""))(emptyStringLast)
        .zip(Iterator.continually(clicksOf))
    }.map{iter =>
      iter.flatMap{ case (ngroup, clicksOf) =>
        val group = ngroup.sorted
        val label = group.head match {
          case regex(title, _) => new Label { text = title }
          case _ => new Separator
        }

        val buttons = group.map{ category =>
          val buttonText = category match {
            case regex(_, subtitle) => subtitle
            case s => s
          }
          new Button {
            text = buttonText
            style = colorOf(clicksOf, category)
            onAction = handle { sub.onNext(Approve(category)) }
            maxWidth = Double.MaxValue
            disable <== tl.sortDisabled.boolProp
          }
        }.toList

        label :: buttons
      }.toList
    }
  }

  def hotkeyHandler(tl: SortingTimeline, str: Subject[StateTransition], ttr: Subject[TimeDirection]) = {
    import KeyCode._

    import MatchKey._
    val undoDisabled = tl.undoDisabled.boolProp
    val redoDisabled = tl.redoDisabled.boolProp
    val isEmpty = tl.sortDisabled.boolProp

    (e: KeyEvent) => e match {
      case combo(DELETE) if !isEmpty.value => str.onNext(Discard)
      case combo(F5) => ttr.onNext(Start)
      case combo(Ctrl, Z) if !undoDisabled.value => ttr.onNext(Backward)
      case combo(Ctrl, Y) if !redoDisabled.value => ttr.onNext(Forward)
      case combo(Ctrl, Shift, Z) if !redoDisabled.value => ttr.onNext(Forward)
      case _ => ()
    }
  }

  def root(tl: SortingTimeline) = new PrimaryStage {
    val str = PublishSubject[StateTransition]()
    val ttr = PublishSubject[TimeDirection]()
    tl.trackChange(str)
    tl.travel(ttr)
    tl.errors.foreach { err =>
      Platform.runLater {
        new Alert(Alert.AlertType.Error) {
          title = "Surprise! Exception!"
          headerText = err.getClass.getSimpleName
          contentText = {
            val sw = new StringWriter
            err.printStackTrace(new PrintWriter(sw))
            sw.toString
          }
        }.showAndWait()
      }
    }

    onCloseRequest = handle { Garbage.cleanUp() }
    title <== tl.fileName.map(fname => s"pic-flat-cat: ${fname.map("sorting " + _).getOrElse("all done!")}").objProp

    scene = new Scene(1000, 800) {
      onKeyPressed = hotkeyHandler(tl, str, ttr)
      root = new HBox {
        maxWidth = Double.MaxValue
        children = Seq(
          new VBox {
            spacing = 10
            padding = Insets(5)
            children = Seq(
              imageView(tl),
              filename(tl),
              progressbar(tl)
            )
            hgrow = Priority.Always
          },
          new VBox {
            spacing = 10
            hgrow = Priority.Never
            children = Seq(
              new VBox {
                hgrow = Priority.Never
                spacing = 10
                padding = Insets(10)
                minWidth = 250
                children <== categorySelector(tl, str).fxChildrenProp
              },
              new Separator,
              toolbar(tl, str, ttr)
            )
          }

        )
      }
    }
  }
}
