package model

import scalax.file.Path

case class SortingState
(
  pending:  Stream[Path],
  total:       Int,
  processed:   Int = 0,
  backlog:     List[Path] = Nil,
  categories:  Seq[String] = Seq.empty,
  clicksOf:    Map[String, Long] = Map.empty.withDefaultValue(0L)
) {
  def current = pending.headOption orElse backlog.headOption
  def skipDisabled = pending.isEmpty
}