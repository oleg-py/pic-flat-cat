package model

import scala.language.implicitConversions
import scalafx.scene.input.{KeyCode, KeyEvent}

object MatchKey {
  sealed trait KeyComboPart
  case object Ctrl extends KeyComboPart
  case object Alt extends KeyComboPart
  case object Shift extends KeyComboPart
  case object Meta extends KeyComboPart
//  case class Key(code: KeyCode) extends KeyComboPart
//  implicit def codeToKey(code: KeyCode): Key = Key(code)

  object combo {
    def unapplySeq(e: KeyEvent): Option[Seq[AnyRef]] = Some({
      val end = Some(e.code)
      @inline def mspec(b: Boolean, sp: => KeyComboPart) = if (b) Some(sp) else None
      val ctrl = mspec(e.controlDown, Ctrl)
      val alt = mspec(e.altDown, Alt)
      val shift = mspec(e.shiftDown, Shift)
      val meta = mspec(e.metaDown, Meta)
      val total = ctrl :: alt :: shift :: meta :: end :: Nil
      total flatMap { x => x }
    })
  }
}
