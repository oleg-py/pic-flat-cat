package model

import scalax.file.{PathMatcher, Path}

import model.timetravel.TimeStarter
import util._

trait SortingLoader extends TimeStarter[SortingState] {
  val Root = Path.fromString(Config.getString("paths.root"))
  private[this] val UnprocessedName = Config.getString("paths.unsorted")
  val Unprocessed = Root / UnprocessedName
  private[this] val IgnorePrefix = Config.getString("paths.ignore-prefix")

  //    def handler = new PathTransitionHandler(Root, leftFiles)

  private[this] def initialState = {
    val (processing, total) = leftFiles()
    SortingState(processing, total, categories = categories)
  }

  private[this] def leftFiles(): (Stream[Path], Int) = {
    val files = Unprocessed.children(PathMatcher.IsFile)
    (files.toStream, files.size)
  }

  private[this] def categories: List[String] = Root
    .children(PathMatcher.IsDirectory)
    .iterator
    .map(p => p.name)
    .filterNot(_ == UnprocessedName)
    .filterNot(_ startsWith IgnorePrefix)
    .toList

  protected def initialize() = initialState
}
