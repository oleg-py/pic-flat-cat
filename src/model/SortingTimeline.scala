package model

import model.timetravel.Timeline
import model.transitions.StateTransition


class SortingTimeline extends Object
  with SortingLoader
  with SortingOperations
  with Timeline[SortingState, StateTransition] {

    val s = states
    val fileName =   s.map(_.current.map(_.name))
    val fileURL =    s.map(_.current.map(_.toURL))
    val processed =  s.map(_.processed)
    val total =      s.map(_.total)
    val cagetories = s.map(point => (point.categories, point.clicksOf))

    val skipDisabled = s.map(_.skipDisabled)
    val sortDisabled = s.map(_.current.isEmpty)
}
