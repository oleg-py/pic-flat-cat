package model

import scala.concurrent.Future
import scala.concurrent.ExecutionContext.Implicits.global
import scalax.file.Path

import com.sun.jna.platform.FileUtils
import model.timetravel.{TimeClock, TimeMachine}
import model.transitions.{Skip, Discard, Approve, StateTransition}
import util.Garbage

trait SortingOperations extends Garbage with TimeClock[SortingState, StateTransition] {
  val Root: Path
  private[this] val RenameConflicting = util.Config.getBoolean("paths.rename-if-exists")

  protected def tick(s: SortingState, ch: StateTransition) = ch match {
    case Approve(cat) => move(s, cat)
    case Discard => discard(s)
    case Skip => skip(s)
  }

  override def cleanUp() = {
    val paths: Array[Path] = tempTrash.children().toArray
    val fsUtils = FileUtils.getInstance()
    if (fsUtils.hasTrash) {
      fsUtils.moveToTrash(paths flatMap {_.fileOption})
    } else {
      paths.foreach(p => p.delete(force = true))
    }
  }

  private[this] def move(state: SortingState, category: String) = new MoveMachine(state, Root / category) {
    val newClicksOf = state.clicksOf.updated(category, state.clicksOf(category) + 1)
    override def forward() = super.forward().map(_.copy(clicksOf = newClicksOf))
  }

  private[this] class MoveMachine(state: SortingState, category: Path) extends TimeMachine[SortingState] {
    private[this] val from = state.current.get
    private[this] val fromBacklog = state.pending.isEmpty
    private[this] val to = {
      val tmp = category / from.name
      if (!RenameConflicting || !tmp.exists) tmp
      else Iterator.from(0)
        .map(i => category / (from.simpleName + "-" + i + from.extension.map("." + _).getOrElse("")))
        .filterNot(_.exists)
        .next()
    }

    override def forward() = Future {
      from.moveTo(to)
      if (!fromBacklog) state.copy(pending = state.pending.tail, processed = state.processed + 1)
      else state.copy(backlog = state.backlog.tail, processed = state.processed + 1)
    }

    override def inverse() = Future {
      to.moveTo(from)
      state
    }
  }

  private[this] val tempTrash = Path.createTempDirectory("pic-flat-cat-temptrash-")
  tempTrash.jfile.deleteOnExit()

  private[this] def discard(state: SortingState) = new MoveMachine(state, tempTrash)

  private[this] def skip(state: SortingState): TimeMachine[SortingState] = new TimeMachine[SortingState] {
    override def inverse() = Future.successful(state)

    override def forward() = Future.successful(
      state.copy(pending = state.pending.tail, backlog = state.pending.head :: state.backlog)
    )
  }
}
