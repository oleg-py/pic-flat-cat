package model

object transitions {
  sealed trait StateTransition
  case class   Approve(category: String) extends StateTransition
  case object  Discard extends StateTransition
  case object  Skip extends StateTransition

}
