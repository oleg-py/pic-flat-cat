package model.timetravel

trait TimeClock[S, C] {
  protected def tick(state: S, change: C): TimeMachine[S]
}
