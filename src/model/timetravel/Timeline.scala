package model.timetravel

import scala.concurrent.Future
import scala.concurrent.ExecutionContext.Implicits.global
import scala.util.{Failure, Success}

import rx.lang.scala.Observable
import rx.lang.scala.subjects.{PublishSubject, BehaviorSubject}

trait Timeline[S, C] {
  this: TimeStarter[S] with TimeClock[S, C] =>
  import direction._

  private[this] val _states = BehaviorSubject(TravelState(initialize(), Nil, Nil))
  private[this] val _errors = PublishSubject[Throwable]()

  case class TravelState(cur: S, fw: List[TimeMachine[S]], bw: List[TimeMachine[S]])

  val states = _states.map(_.cur)
  val undoDisabled = _states.map(_.fw.isEmpty)
  val redoDisabled = _states.map(_.bw.isEmpty)
  val errors: Observable[Throwable] = _errors

  def trackChange(transitions: Observable[C])  =
    transitions.withLatestFrom(_states)(Tuple2(_, _)) foreach { case (tr, TravelState(state, fw, bw)) =>
      val machine = tick(state, tr)
      machine.forward().onComplete {
        case Success(value) => _states.onNext(TravelState(value, machine :: fw, Nil))
        case Failure(ex) => _errors.onNext(ex)
      }
    }

  def travel(controls: Observable[TimeDirection]) =
    controls.withLatestFrom(_states)(Tuple2(_, _)) foreach { case (tc, TravelState(state, fw, bw)) =>
      val result = tc match {
        case Forward =>
          val machine :: rest = bw
          machine.forward() map { value => TravelState(value, machine :: fw, rest) }

        case Backward =>
          val machine :: rest = fw
          machine.inverse().map { value => TravelState(value, rest, machine :: bw) }

        case Start => Future.successful(TravelState(initialize(), Nil, Nil))
      }

      result.onComplete {
        case Success(value) => _states.onNext(value)
        case Failure(ex) => _errors.onNext(ex)
      }
    }
}