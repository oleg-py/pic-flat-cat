package model.timetravel

object direction {
  sealed trait TimeDirection
  case object  Start extends TimeDirection
  case object  Forward extends TimeDirection
  case object  Backward extends TimeDirection
}