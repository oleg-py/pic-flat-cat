package model.timetravel

import scala.concurrent.Future

trait TimeMachine[S] {
  def forward(): Future[S]
  def inverse(): Future[S]
}