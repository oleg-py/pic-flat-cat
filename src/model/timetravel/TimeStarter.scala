package model.timetravel

trait TimeStarter[S] {
  protected def initialize(): S
}