import scalafx.application.JFXApp

import model.SortingTimeline
import view._

object Runner extends JFXApp {
  val timeline = new SortingTimeline
  stage = components.root(timeline)
}
